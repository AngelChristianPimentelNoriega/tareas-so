Equipo: Los Jimbos
Integrantes:
	Paola Betsabe Barredo Escalona
	Diego Alonso Medina Amayo
	Angel Christian Pimentel Noriega

Para correr de manera correcta el proyecto es necesario conservar el orden de los archivos, es decir, conservar el _Makefile_ a la altura de _src_ y dentro del _src_ debe de ir el archivo de main.c. Con lo anterior necesitamos simplemente escribir a la altura de _equipo_Los_Jimbos_ los siguiente:
_make_

_sudo insmod ./src/main.ko {argumentos para los tres parametros}_

_cat  /var/log/kern.log | grep ID_ 

Obtenemos el ID de el Char Device

_sudo mknod -m 0666  /dev/jimbodevice c {Major ID char device} 0_


El módulo toma 3 posibles parámetros:
- El primero, llamado "entero" es un entero sin signo de 8 bits, este parámetro tiene prioridad sobre los otros parámetros.
- El segundo, llamado "cadena" es un arreglo de caracteres de máximo 32 elementos, este tiene prioridad sobre el último parametro.
- El tercero, de nombre arreglo es un arreglo de enteros de máximo
5 elementos.

El módulo registra un nuevo dispositivo de caracteres en el kernel, e imprime el número asignado para ese dispositivo en el log del kernel.
El módulo elimina el dispositivo de caracteres del kernel antes de ser eliminado del kernel y solo si no hay nadie usándolo.
El módulo implementa las funciones open, release, write y read como estan definidas en la estructura file_operations del encabezado linux/fs.h

Caracteristicas Funcionales:
-Uso correcto de las funciones y encabezados del kernel
-Uso de parametros en el módulo
-Soporte de las funciones open, release, read y write.
-Uso correcto de memoria (no deben haber fugas de memoria)
-Manejo de errores
-Makefile funcional
-Comentarios en el código

Bugs conocidos:
-No es posible correr el código sin correct desactivar el lockdown de algunos sistemas operativos para integrar modulos de terceros

