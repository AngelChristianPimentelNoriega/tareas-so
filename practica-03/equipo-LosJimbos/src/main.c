#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/init.h>
#include <linux/moduleparam.h>
#include <linux/stat.h>
#include <linux/fs.h>
#include <asm/uaccess.h>	

#define DEVICE_NAME "jimbodevice"
#define BUFFER_LENGTH 250



//La variable donde guardamos el mensaje al usuario
static char message[BUFFER_LENGTH];
//Esta variable itera a message al llamar al metodo device_read
static char *message_iterator;
static int device_id;
//Bandera que nos ayuda a señalar si un usuario esta accediendo
// actualmente a el char device.
static int device_is_open = 0;


static int device_open(struct inode *, struct file *);
static int device_release(struct inode *, struct file *);
static ssize_t device_read(struct file *, char *, size_t, loff_t *);
static ssize_t device_write(struct file *, const char *, size_t, loff_t *);

/*Función definida en linux/fs.h, contiene apuntadores a las funciones definidas
por el driver que realiza varias operaciones en el device*/
static struct file_operations file_operation = {
	.read = device_read,
	.write = device_write,
	.open = device_open,
	.release = device_release
};

/* Función que establece conexión con el device*/
static int device_open(struct inode *inode, struct file *file)
{
    //Comprueba que no haya otro usuario accediendo al char device
	if(device_is_open)
        return -EBUSY;
    //Activa la bandera  
    device_is_open++;

    message_iterator = message;
    try_module_get(THIS_MODULE);

    return 0;
}

/* Función que finaliza la conexión del usuario con el device*/
static int device_release(struct inode *inode, struct file *file)
{
    //Al terminar la conexion desactivamos la bandera
    device_is_open--;
    module_put(THIS_MODULE);
    return 0;
}

/*Función que lee bytes_wanted bytes del device y 
los almacena en un búfer asignado con vm_allocate,
cuya dirección se devuelve en los datos.
El número de bytes realmente devuelto se almacena en data_count. */
static ssize_t device_read(struct file *filp, char *buffer,	size_t length, loff_t * offset)
{
    int bytes_read = 0;
    //llegamos al final
    if (*message_iterator == 0)
		return 0;
    //recorremos el mensaje
    while (length && *message_iterator) {
        put_user(*(message_iterator++), buffer++);

		length--;
		bytes_read++;
    }
	return bytes_read;
}

/* La función device_write escribe bytes de recuento 
de datos desde el búfer de datos al device. 
El número de bytes realmente escritos se devuelve en bytes_written */
static ssize_t device_write(struct file *filp, const char *buff, size_t len, loff_t * off)
{
    //No soportada, manda un mensaje de error.
	printk(KERN_ALERT "No se puede escribir al archivo.\n");
	return -EINVAL;
}


//Parametros de entrada.

int entero = 0;
module_param(entero, int, 0);
MODULE_PARM_DESC(entero, "Número a imprimir");

char *cadena = "";
module_param(cadena, charp, 0);
MODULE_PARM_DESC(cadena, "Cadena a imprimir");

int arreglo[5];
int numero_elementos;
module_param_array(arreglo, int, &numero_elementos, 0);
MODULE_PARM_DESC(arreglo, "Arreglo a imprimir");

/*
    En esta funcion establecemos la prioridad de los parametros
    Si no son mandados un mensaje de error es mandado
    Guarda el parametro elegido en la variable [message]
*/
int check_params(void){
    int i;
    if (entero == 0){
        if (strlen(cadena) == 0 ){
            if(numero_elementos == 0){
                printk(KERN_ALERT "Tienes que introducir al menos uno de los valores\n");
                return -1;
            }else{
                strcat(message, "Arreglo : [");
                for (i = 0; i < numero_elementos; i++){
                    char valor_arreglo[40];
                    if(i < numero_elementos -1 )
                        sprintf(valor_arreglo, "%d,", arreglo[i]);
                    else
                        sprintf(valor_arreglo, "%d", arreglo[i]);
                    strcat(message, valor_arreglo);
                }
                strcat(message, "]");
            }             
        }else{
            if(strlen(cadena) > 32){
                printk(KERN_ALERT "El tamaño máximo es de 32 : %s\n", cadena);
                return -1;
            }
            strcpy(message, "Cadena: ");
            strcpy(message, cadena);
        }
    }else{
        sprintf(message, "Entero: %d", entero);
    }
    return 0;
}

/* La función mod_init acepta los datos de estado bajo para el estado de ejecución 
dado como argumento. Devuelve un booleano. Si devuelve True,  la función no se 
ejecuta nuevamente, oues se ha configurado el comportamiento necesario.
En otro caso, la función se llamará la próxima vez*/
int __init mod_init(void) { 
    if(check_params() == -1)
        return -1;
    device_id = register_chrdev(0, DEVICE_NAME, &file_operation);
    if (device_id < 0) {
        printk(KERN_ALERT "No se pudo registrar el dispositivo de caracteres con id: %d\n", device_id);
        return -1;
	}
    printk("ID del dispositivo de caracteres [%s]: %i\n",DEVICE_NAME,device_id);
    printk("Mensaje %s\n", message);
    return 0;
}

void __exit mod_cleanup(void) {
    unregister_chrdev(device_id, DEVICE_NAME);
}


module_init(mod_init);
module_exit(mod_cleanup);

//Licencia del módulo
MODULE_LICENSE("GPL");
//Nombre de los autores
MODULE_AUTHOR("Paola Betsabe Barredo Escalona,Diego Alonso Medina Amayo,Angel Christian Pimentel Noriega ");
//Descripción del módulo
MODULE_DESCRIPTION("Modulo de Kernel de Linux creado para la practica 3 de el curso de Sistemas Operativos 2021-2");

