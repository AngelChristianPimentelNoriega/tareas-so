Equipo: Los Jimbos
Integrantes:
	Paola Betsabe Barredo Escalona
	Diego Alonso Medina Amayo
	Angel Christian Pimentel Noriega

Como ejecutar:
Para ejecutar el programa es necesario conservar la estructura del repositorio, una vez que se tienen los archivos en su totalidad (main.c, index.html y el Makefile) es necesario situarse en el directorio de "equipo-LosJimbos" y escribir el comando _make_ (Sin incluir guiones bajos), bastará con ésto para que el programa sea ejecutado y corra.
Una vez se encuentre el archivo corriendo, podemos ingresar en cualquier navegador a la dirección _127.0.0.1:8080/index.html_, dicha acción será interpretada como una solicitud GET.
Para agregar un archivo al que se deseé acceder es necesrio ingresar dicho archivo a la carpeta _./static_


Características no Implementadas:
	No se pueden subir a directorios encima del root path establecido por el usuario
	Se mantiene la conexión con el cliente hasta que se reciba la indicación de cerrarla con un encabezado
	Connection: close o se hayan recibido más de 5 peticiones del mismo cliente.

	De los errores y su registro:

	Todas las funciones que señalicen un error deben son manejadas al menos imprimiendo el error en stderr y en caso
	de ser necesario terminando el proceso.
	Todas la conexiones nuevas, peticiones y respuestas se imprimen en stderr (solo request-line y status line)
	(Puntos Extras) En lugar de imprimir en stderr imprimir en el syslog del sistema

Características Implementadas:

De las urls aceptadas:


Se aceptan aceptar urls de archivos y servirlos si la ruta esta dentro del root path

Si la url es una carpeta se sirve el archivo por defecto index.html


De la conexión con los clientes:


Cuando el servidor cierra una conexión con un cliente se envia el encabezado Connection: close en la
siguiente respuesta que envíe al cliente.



Respuestas a los clientes:

Bien formadas de acuerdo al RFC7230 y los demás RFCs pertinentes
Todas las respuesta especifican la fecha y hora en el formato y zona horaria adecuadas
Si se sirve un archivo se especifica:

MIME Type
Tamaño del archivo servido


De las peticiones GET:

Las peticiones mal formadas son respondidas con la respuesta adecuada (400 Bad Request).
Las peticiones que no se puedan completar son respondidas con la respuesta adecuada (403 Forbidden
, 404 Not Found o 501 Not Implemented).
(Puntos extra) Implementar peticiones HEAD



De los procesos lanzados:

Una vez lanzado un nuevo procesos hijo se cierra el socket del servidor
Una vez lanzado un nuevo proceso hijo el padre se cierra el socket obtenido de accept

Cuando se envie una señal al proceso padre de finalización los procesos hijos deben termina de forma limpia,
cerrando archivos, liberando memoria y cerrando conexiones








