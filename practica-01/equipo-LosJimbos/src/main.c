#include <stdio.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdbool.h>
#include <sys/stat.h>
#include <time.h>
#include <sys/wait.h>
#include <signal.h>
#include <unistd.h>

#define LISTEN_PORT 8080
#define REQUEST_SIZE 250
#define STATIC_FOLDER "/src/static"
#define NUMERO_PROCESOS 4

/**
 * Abre un socket de tipo ipv4, se conecta a él y lo pone a escuchar
 *
 *
 * @param listen_port El puerto donde va a estar escuchando
 *               
 */
int open_tcp_soc(int listen_port)
{
    int sd;
    sd = socket(AF_INET, SOCK_STREAM, 0);
    struct sockaddr_in server_addr;
    server_addr.sin_family = AF_INET;
    server_addr.sin_addr.s_addr = htons(INADDR_ANY);
    server_addr.sin_port = htons(listen_port);
    if ((bind(sd, (struct sockaddr *)&server_addr, sizeof(server_addr))) < 0)
        return -1;
    if (listen(sd, 10) != 0)
        return -1;
    return sd;
}

/**
 * Obtiene los headers de el cliente usando la funcion recv
 *

 *
 * @param socket Int nuestro socket donde escuchamos requests
 * 
 * @param buf Apuntador donde vamos a guardar los headers del cliente
 * 
 * @param len La longitud de los headers del cliente
 *              
 */
int receive_message(int socket, char *buf, int len)
{
    char *client_data = buf;
    int slen = len;
    int c = recv(socket, client_data, slen, 0);
    if (c < 0)
        return c;
    else if (c == 0)
        buf[0] = '\0';
    else
        client_data[c] = '\0';
    return len - slen;
}

/**
 * Manda un string al socket usando la funcion send
 *

 *
 * @param socket Int nuestro socket donde escuchamos requests
 * 
 * @param message String El mensaje que vamos a transmitir
 * 
 *              
 */
bool send_string(int socket, char *message)
{
    int numSent = send(socket, message, strlen(message), 0);
    if (numSent <= 0)
    {
        if (numSent == 0)
        {
            printf("No se estableció conexión con: desconectado\n");
        }
        else
        {
            perror("No se establecióo conexión con");
        }
        return false;
    }
    return true;
}

/**
 * Manda un archivo al socket usando la funcion send
 *
 *
 * @param socket Int nuestro socket donde escuchamos requests
 * 
 * @param senddata String El archivo que vamos a transmitir
 * 
 * @param datalength long La longitud del archivo a transmitir
 *              
 */
bool send_file(int socket, const void *senddata, long int datalength)
{
    const char *pdata = (const char *)senddata;

    while (datalength > 0)
    {
        int numSent = send(socket, pdata, datalength, 0);
        if (numSent <= 0)
        {
            if (numSent == 0)
            {
                printf("No se estableció conexión con: desconectado\n");
            }
            else
            {
                perror("No se establecióo conexión con");
            }
            return false;
        }
        pdata += numSent;
        datalength -= numSent;
    }

    return true;
}

/**
 * Usando los headers de la petición del cliente obtiene el metodo HTTP
 * 
 * @param message String Los headers del cliente
 *               
 */
char *get_method(char *message)
{
    char *copied_message = (char *)malloc(REQUEST_SIZE);
    strcpy(copied_message, message);
    return strtok(copied_message, " ");
}

/**
 * Usando los headers de la petición del cliente obtiene la ruta del archivo 
 * 
 * @param message String Los headers del cliente
 *               
 */
char *get_file_name(char *message)
{
    char *copied_message = (char *)malloc(REQUEST_SIZE);
    strcpy(copied_message, message);
    char *token = strtok(copied_message, " ");
    token = strtok(NULL, " ");
    return token;
}

/**
 * Lee un archivo linea por linea y lo guarda en un string
 *
 *
 * @param filename String el nombre del archivo
 * 
 * @param size Apuntador hacia el long que será actualizado en la funcion.
 * 
 *              
 */
char *read_file(char *filename, long *size)
{
    //Esta operación es necesaria ya que la concatenación
    // manda un core dumped por falta de memoria.
    long *filesize = size;
    char current_directory[250];
    getcwd(current_directory, sizeof(current_directory));
    int length = 250 + strlen(STATIC_FOLDER) + strlen(filename);
    //Variable de tamaño dinámico
    char *path = malloc(length);
    strcpy(path, current_directory);
    strcat(path, STATIC_FOLDER);
    strcat(path, filename);

    FILE *fp = fopen(path, "rb");
    if (!fp)
    {
        perror("El archivo no ha sido abierto");
        exit(1);
    }

    printf("El archivo ha sido abierto\n");

    if (fseek(fp, 0, SEEK_END) == -1)
    {
        perror("El archivo no fue solicitado");
        exit(1);
    }
    //Se actualiza el tamaño del archivo, será usado en main

    *filesize = ftell(fp);
    if (*filesize == -1)
    {
        perror("El tamaño del archivo no fue recibido");
        exit(1);
    }
    //Regresa al inicio de el archivo
    rewind(fp);

    //
    char *file_content = (char *)malloc(*filesize);
    if (!file_content)
    {
        perror("No se encontro el bufer del archivo\n");
        exit(1);
    }

    if (fread(file_content, *filesize, 1, fp) != 1)
    {
        perror("No se leyó el archivo\n");
        exit(1);
    }
    fclose(fp);

    return file_content;
}
/**
 * Fucnión para obtener el tiempo
 */
char* get_current_time()
{
    static char buf[250];
    time_t now = time(0);
    struct tm times = *gmtime(&now);

    strftime(buf, sizeof buf, "Date: %a, %d %b %Y %H:%M:%S %Z\r\n", &times);

    return buf;
}
/**
 * Manda el header de la respuesta HTTP
 *
 *
 * @param socket Int nuestro socket donde escuchamos requests
 * 
 * @param httpmessage String El mensaje y codigo de http
 * 
 * @param mime_type String El tipo de archivo 
 *              
 */
void send_headers(int socket, char *httpmessage, long filesize, char *mime_type)
{
    printf("%s", httpmessage);
    send_string(socket, httpmessage);
    send_string(socket, get_current_time());
    char contentlength[40];
    sprintf(contentlength, "Content-length: %li\r\n", filesize);
    send_string(socket, contentlength);
    char contenttype[40];
    sprintf(contenttype, "Content-Type: %s\r\n", mime_type);
    send_string(socket, contenttype);
    send_string(socket, "Connection: close\r\n\r\n");
}
/**
 * Usando la función stat comprueba si es un directorio
 * 
 * @param filename String El nombre del archivo
 * 
 *              
 */
int is_directory(const char *filename)
{
    //Esta operación es necesaria ya que la concatenación
    // manda un core dumped por falta de memoria.
    char current_directory[250];
    if(getcwd(current_directory, sizeof(current_directory)) == NULL)
        return 0;
    int length = 250 + strlen(STATIC_FOLDER) + strlen(filename);
    //Variable de tamaño dinámico
    char *path = malloc(length);
    strcpy(path, current_directory);
    strcat(path, STATIC_FOLDER);
    strcat(path, filename);

    struct stat statbuf;
    if (stat(path, &statbuf) != 0)
        return 0;
    return S_ISDIR(statbuf.st_mode);
}

/**
 * Comprueba si el archivo existe
 * 
 * @param filename String El nombre del archivo
 * 
 * @return 1 Existe
 * @return 0 No existe             
 */
int file_exists(char *filename)
{
    //Esta operación es necesaria ya que la concatenación
    // manda un core dumped por falta de memoria.
    char current_directory[250];
    if(getcwd(current_directory, sizeof(current_directory)) == NULL)
        return 0;
    int length = 250 + strlen(STATIC_FOLDER) + strlen(filename);
    //Variable de tamaño dinámico
    char *path = malloc(length);
    strcpy(path, current_directory);
    strcat(path, STATIC_FOLDER);
    strcat(path, filename);


    if( access( path, F_OK ) == 0 ) 
        return 1;
    return 0;

}

/**
 * Obtiene el mimetype usando la extension del archivo
 * 
 * @param filename String El nombre del archivo
 * 
 * @return El tipo de archivo       
 */
char *get_mime_type(char *filename)
{
    //Split usando como delimitador ".", utiliza el 2do token
    char *copied_filename = (char *)malloc(strlen(filename));
    strcpy(copied_filename, filename);
    char *extension = strtok(copied_filename, ".");
    extension = strtok(NULL, ".");
    //Mimetype solo funciona si esta dentro de estos casos, sino regresa text/plain
    if (strcmp(extension, "html") == 0)
    {
        return "text/html";
    }
    if (strcmp(extension, "gif") == 0)
    {
        return "image/gif";
    }
    if (strcmp(extension, "jpeg") == 0 || strcmp(extension, "jpg") == 0)
    {
        return "image/jpg";
    }
    if (strcmp(extension, "png") == 0)
    {
        return "image/png";
    }
    if (strcmp(extension, "css") == 0)
    {
        return "text/css";
    }
    if (strcmp(extension, "js") == 0)
    {
        return "application/javascript";
    }
    if (strcmp(extension, "json") == 0)
    {
        return "application/json";
    }
    if (strcmp(extension, "txt") == 0)
    {
        return "text/plain";
    }
    return "text/plain";
}
/**
 * Reduce el numero de procesos activos usando la señal SIGURS1
 * 
 * @param signal
 * 
 *        
 */
int procesos = 0;
void reduce_childs(int signal)
{
    procesos--;
}
/**
 * Cuando el numero de procesos hijos sea cero termina el programa SIGINT
 * 
 * @param signal 
 * 
 *        
 */
void father_proccess(int signal)
{
    while (wait(NULL) > 0)
    {
        continue;
    }
    exit(0);
}

/**
 * Main
 * Abre socket
 * Informa al usuario sobre la configuracion
 * Escucha nuevas conexiones
 * Recibe los datos de conexiones
 * Filtra los metodos permitidos
 * Envia la response del servidor
 * 
 *        
 */
int main()
{
    signal(SIGINT, father_proccess);
    printf("Webserver creado por los Jimbos, iniciando...\n");
    int webserver;
    webserver = open_tcp_soc(LISTEN_PORT);
    printf("Webserver escuchando en la dirección 127.0.0.1 en el puerto %d\n", LISTEN_PORT);
    printf("ROOT_PATH: %s\n", STATIC_FOLDER);
    printf("Numero de procesos: %i\n", NUMERO_PROCESOS);
    int new_connection;                            //Conexion del cliente
    char *message = (char *)malloc(REQUEST_SIZE);  //Headers del cliente
    long *filesize = (long *)malloc(sizeof(long)); //Tamaño del archivo a enviar
    struct sockaddr_storage client_addr;           //Datos del cliente
    unsigned int address_size = sizeof(client_addr);
    printf("Esperando conexión:\n");
    while (1)
    {

        signal(SIGUSR1, reduce_childs);

        new_connection = accept(webserver, (struct sockaddr *)&client_addr, &address_size);
        struct sockaddr_in *coming = (struct sockaddr_in *)&client_addr;
        unsigned char *ip = (unsigned char *)&coming->sin_addr.s_addr;
        unsigned short port = coming->sin_port;
        if (new_connection < 0)
        {
            // Error de conexion
            printf("Intento fallido de conexión con el cliente: %d.%d.%d.%d:%d\n", ip[0], ip[1], ip[2], ip[3], port);
            continue;
        }

        if (procesos >= NUMERO_PROCESOS)
        {
            printf("Se llego al limite de procesos");
            send_headers(new_connection, "HTTP/1.1 503 - Service Unavailable\r\n", 0, "text/plain");
            close(new_connection);
            continue;
        }

        printf("Conexión exitosa con el cliente: %d.%d.%d.%d:%d\n", ip[0], ip[1], ip[2], ip[3], port);
        
        //Fork regresa 0 y se crea el nuevo proceso hijo
        //Fork regresa negativo si algo salio mal
        //Fork regresa positivo cuando se llama desde un proceso padre y regresa el process id del hijo
        pid_t pid = fork();
        procesos++;
        if (pid < 0) 
        {
            printf("El servidor no pudo crear un proceso hijo.\n");
            printf("Conexion Cerrada");
            procesos--;
            continue;
        }
        else if (pid > 0)
        {
            //Se cierra el proceso padre
            close(new_connection);
            continue;
        }
        else 
        {
            close(webserver);
            

            //Recibimos los datos del cliente(message)
            int receive_num = receive_message(new_connection, message, REQUEST_SIZE);

            char *method = get_method(message);
            printf("HTTP Method: %s\n", method);
            char *filename = get_file_name(message);
            printf("Archivo Solicitado: %s\n", filename);
            //Hay veces en las que el navegador manda requests nulas
            if (method == NULL || filename == NULL)
            {
                send_headers(new_connection, "HTTP/1.1 400 BAD REQUEST\r\n", 0, "text/plain");
                close(new_connection);
                kill(getppid(), SIGUSR1);
                exit(0); 
                continue;
            }

            //Comprobamos si se pudieron obtener datos del cliente
            if (receive_num < 0)
            {
                perror("No se pudieron leer los datos del cliente");
                close(new_connection);
                kill(getppid(), SIGUSR1);
                exit(0); 
                continue;
            }
    
            //Comprobamos metodos
            if (strcmp(method, "HEAD") == 0)
            {
                if (is_directory(filename))
                {
                    read_file("/index.html", filesize);
                    send_headers(new_connection, "HTTP/1.1 200 OK\r\n", *filesize, "text/html");
                    close(new_connection);
                    kill(getppid(), SIGUSR1);
                    exit(0); 
                    continue;
                }
                if (file_exists(filename) == 0)
                {
                    send_headers(new_connection, "HTTP/1.1 404 NOT FOUND\r\n", 0, "text/plain");
                    close(new_connection);
                    kill(getppid(), SIGUSR1);
                    exit(0); 
                    continue;
                }
                char *mime_type = get_mime_type(filename);
                read_file(filename, filesize);
                send_headers(new_connection, "HTTP/1.1 200 OK\r\n", *filesize, mime_type);
            }
            else if (strcmp(method, "GET") == 0)
            {
                if (is_directory(filename))
                {
                    char *filecontent = read_file("/index.html", filesize);
                    send_headers(new_connection, "HTTP/1.1 200 OK\r\n", *filesize, "text/html");
                    send_file(new_connection, filecontent, *filesize);
                    close(new_connection);
                    kill(getppid(), SIGUSR1);
                    exit(0); 
                    continue;
                }
                if (file_exists(filename) == 0)
                {
                    send_headers(new_connection, "HTTP/1.1 404 NOT FOUND\r\n", 0, "text/plain");
                    close(new_connection);
                    kill(getppid(), SIGUSR1);
                    exit(0);    
                    continue;
                }
                char *mime_type = get_mime_type(filename);
                char *filecontent = read_file(filename, filesize);
                send_headers(new_connection, "HTTP/1.1 200 OK\r\n", *filesize, mime_type);
                send_file(new_connection, filecontent, *filesize);
                printf("Archivo transferido exitosamente\n");
            }
            else
            {
                send_headers(new_connection, "HTTP/1.1 501 NOT IMPLEMENTED\r\n", 0, "text/plain");
            }
            printf("Conexión finalizada.\n");
            kill(getppid(), SIGUSR1);
            exit(0);

        }
    }
}
